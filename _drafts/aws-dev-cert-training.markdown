---
layout: post
title: AWS Dev Cert Training
---

## IAM

Groups and users
- perms to groups and users in group has perms
- 
Roles and policies
- roles you assign policies to

Calls to API restricted by tokens. and api's, lambda assume a role

can integrate to federated access eg AD


Many cases policies and roles created by the api.

### Role and policies

Policy - perms
    Role - collection of policies
    Perms for users, code and services, can defined own policy. 
     Can filter polices - eg s3fullaccess
     Can create roles to join things together eg a lamba to read from s3 and write to dynamodb with policies.
    
### Users and groups

Create group assign policies and add group to user.
    
Access type programatic or console (in which case they need a password)

## Cognito

User pools and identity poolse
User pools for user directories for signup and signin for app users
Id pools for aws creds to grant access to aws services

User pools need to define username, password MFA etc
Identity pools may not need to be logged in.  For apps to gain access


Once defined a userpools is created and then users are added to it. Users that can create an account within the app.

Ident pools cognito sync data (now AppSync)
Manage identity pool -> pool name -> enable unauth access, auth providers - includes SAML and openid.

Open ID access 

SAML ID access

In IAM need to create a provider eg SAML

Need to register AWS to SAML Provider. Get the meta doc and configure IdP with AWS and well (IAM). In configuring the IdP in AWS you need to add role and policy.

#### Flow 
Log in to existing account get SAML assertion -> AssumeRoleWithSAML call 
Call STS (AWS secure token service) with AssumeRoleWithSAML and STS returns Creds to use in AWS calls


OpenID connect

IAM -> ident provider -> open id connect config -> 

Flow 

Login to OpenID server to get access token and use assumerolewithwebidentity for temp credentionals passing the access tokens.

Can check with it on Web Ident Feb playground, sign it and get the access token.

Create app presence with IdP 
Create IdP in IAM

## Building

SDKs and Toolkits



### S3 - Simple Storage Service

Create bucket


### Dynamo Db

### Lambdas
From Scratch, blueprints or Serverless Application Repo

### SQS - Simple Queue Service

Decouple and scale.

SSE - server side encryption
KMS - Key management service

2 Qs - Standard. Max throput, best effort ordering, at lest once delivery
FIFO Qs, processed only once and in order

Add ARN to trigger to attach to Lambda


SQS - stores until some reads it (P2P)

### SNS - Simple Notification Service

Push to Subs

Can get SQS to sub to SNS topic


So push message to SNS -> SQS -> Lambda

Can create triggers and subscribers either way


### Triggers
DynamoDb -> Tables -> MYData -> Triggers
Batch size? Records updated. Create trigger needs permissions.
Change roles
IAM -> ROles -> pick role (the lambda role) and add dynamodb policy

SO when entries are added to the table lambda will files



## Step Functions


Tasks and State Machines

Tasks are Code (Lambda)

States - relationships on input and output

Step functions -> syncing backing up S3 bucket, email verification, authorization of process

Scaling image automation
 
Step Functions -> get started.

Will create lambdas -etc in a state function


## Elastic Beanstalk

Scale webapps and services
EC2 instance running Lx or Win
Secures port 80 for http ingress (needs VPC)
Auto scaling group
S3 bucket for source code logs etc
Adds cloudwatch alarms 
Domains names  subdomain.region.elasticbeanstalk.com

Steps: select platform, upload app, run


What about IaC CI pipeline -> CD ? 

EBs creates a cloud formation stack which help to configure and maintain the system

Cloud formation stack template designer -> visual/json 

###  Elastic Cache

Distributed in memory cache.
 
 Redis or memcached
 
 subnets, zones,security group

Lambda - use the cache, new role and create function, the lambda needs the elastic cache arn. Need to add vpc policy to role.

Security group needs ports enabled.

## Cloud Front

Global CDN
S3 bucket (add eg index.html)
In cloud front create a web distribution (to pull from S3?)

## Cloud Watch

Log file, aws monitoring, insights, events, alarms, dashboards etc

Monitor costs, s3 buckets etc


# Security

IAM, Storage, Encryption, Key Management Services, Cognito 

IAM Console
- Identity - resource user role to grant permissions
- Policies - access grant
- Users
- Groups eg dbs, admins 
- Role to apps and policies to role.
- To access services you need access key pair
- Users -> add -> name -> aws mgmt console access, -attach policies - admin policies
- Trust policy - who can have this role, eg users, federated users, web idenity users and aws services.
-- Role helps separate production and test environments 


Key points

always use the IAM users instead of root and have MFA set
Use groups rather than individual users
Policy is defaulted to deny all
Do not hardcode access keys in code


s3 security
Use bucket to host web site
Need bucket policy to allow access to objects in bucket
Can encrypted the data with AES-256 or AWS-KMS (they manage the keys).
S3 versioning - to roll back to a different version

Lifecycle rule to delete older version - on expiratoin - eg 30 days
IAM users can add additional MFA


Key points

Bucket tools to enable/restrict access to S3
Data encryption in transit is SSL/TLS
Data encryption at rest can use S3 managed keys  

KMS

Encryption key store, can import keys as wells

IAM -> click encryption keys -> region specific, create key (can tag with environment).

Key rotation


Cognito
Identity provider and broker 

AWS Config - tool to help with governance - rules and compliance. Monitor for rules

AWS Trusted Advisor -scan and help improve

AWS Cloud trail - audit changes to account










 
 
 



 




    




