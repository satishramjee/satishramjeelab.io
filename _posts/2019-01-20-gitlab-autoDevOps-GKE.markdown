# Gitlab Auto DevOps with GKE

## Simple Go Service

### Go file
Note: Needs to be on port 5000

```go
package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, you've requested: %s\n", r.URL.Path)
	})

	log.Print("Started")
	http.ListenAndServe(":5000", nil)
}
```

### DockerFile

```
# build stage
FROM golang:alpine AS build-env
ADD . /src
RUN cd /src && go build -o goapp

# final stage
FROM alpine
WORKDIR /app
COPY --from=build-env /src/goapp /app/
ENTRYPOINT ./goapp

```

## K8 & Gitlab integration

Operations > Kubernetes > create cluster on Google Cloud with your account. 

Give it a name.

Environment can be wildcard.

Ideally you would have three envs:
- Review
- Stage
- Prod

For this example you only need 1 node

Once the cluster is up you need to install:
- Helm Tiller
- Ingress
- Prometheus
- Gitlab runner
- Knative (see later)

Machine type:
n1-standard-1 (1 vCPU, 3.75 GB memory)

Once you have ingress up and running you will have an IP

Use the wildcard DNS service from nip.io to give you a domain name and add it to gitlab settings > CI/CD -> Auto DevOps.
eg: 

`104.155.130.88.nip.io`

Select auto devops pipeline.

Turn off Test, Code Quality, Postgres jobs:

In Settings > General > Environment Variables:

- TEST_DISABLED
- CODE_QUALITY_DISABLED
- POSTGRES_ENABLED


Once the code is pushed the jobs build and production will run and give you an endpoint to test

Gitlab > Environmets.

Here you can launch the endpoint, view the monitor and get a shell to the system.

```
http://{clustername}.104.155.130.87.nip.io/test
```

Should return:
```
Hello, you've request: /test
```

### GKE

GCP > Kubernetes Engine > Clusters

This gives a view of the node pools.

Also 

GCP > Compute Engine > VM instances

To view the actual VMs.

Here you can ssh into the VM. Also look at logs in Stack driver logging.



## Knative
Extends K8 divided into:
- Serving, request driven compute
- Build, Source to container orchestration
- Eventing, managing delivery of events

Abstracts common tasks:
- Container deployment
- Routing and managing traffic
- Autoscale (including to 0)
- Binding to events


Solves:
- Platform agnostic
- Simplifies operational concerns
- Focus on writing code


Appears to needs a minimum of 3 nodes.

Lets try with 3 and n1-standard-1

Do not need to install ingress, just install knative and use the IP it gives. For domain use nio.io.

Production passes but the IP does not work. Missing something perhaps this is side to side access.

Install ingress and change the auto dev ops to point to the ingress IP.

Ok this is better now we get invalid host header (vue application), the dev server in vue isn't the right one, ought to build it properly.

Had to add  vue.config.js to root and add  host port settings:

```javascript
module.exports = {
    devServer: {
        host: '0.0.0.0',
        port: '5000',
        public: 'appointz-view.35.231.84.38.nip.io',
    },
}
```


-- and it works.







